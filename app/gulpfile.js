/* Requiring necessary packages */
var gulp = require('gulp'),
		autoprefixer = require('gulp-autoprefixer'),
		browsersync = require('browser-sync'),
		changed = require('gulp-changed'),
		concat = require('gulp-concat'),
		cssnano = require('gulp-cssnano'),
		uglify = require('gulp-uglify'),
		imagemin = require('gulp-imagemin'),
		sass = require('gulp-sass');

/* Setting base project constants */
const paths = {
	src: './src/',
	dest: '../'
};

/* Setting an error swallower */
var swallowError = function(error) {
	console.log(error.toString())
	this.emit('end')
}

/*
* BASIC
*
* Those listed below are the basic tasks
* for compiling & distributing files
*/
gulp.task('php', function() {
	gulp.src([paths.src + '**/*.php', paths.src + '**/*.html'])
		.pipe(changed(paths.dest))
		.pipe(gulp.dest(paths.dest));
});

gulp.task('css', function() {
	gulp.src([paths.src + 'scss/**/*.scss'])
		.pipe(changed(paths.dest))
		.pipe(sass())
		.on('error', swallowError)
		.pipe(autoprefixer())
		.pipe(cssnano({zindex: false}))
		.pipe(concat('style.min.css'))
		.pipe(gulp.dest(paths.dest + 'css'));
});

gulp.task('js', function() {
	gulp.src([paths.src + 'js/**/*.js'])
		.pipe(changed(paths.dest + 'js'))
		.pipe(uglify())
		.pipe(concat('scripts.min.js'))
		.pipe(gulp.dest(paths.dest + 'js'));
});

gulp.task('img', function() {
	// Setting allowed images
	gulp.src([
		paths.src + 'img/*.jpg',
		paths.src + 'img/*.gif',
		paths.src + 'img/*.png',
		paths.src + 'img/*.svg',
		paths.src + 'img/*.mp4'
	])
		.pipe(changed(paths.dest + 'img'))
		.pipe(imagemin())
		.pipe(gulp.dest(paths.dest + 'img'));
});

gulp.task('libs', function() {
	/* 
	* Here comes all the third-party files
	* like Fontawesome, bulma...
	*/

	// CSS Libs
	gulp.src([
		'node_modules/animate.css/animate.min.css',
		'node_modules/swiper/dist/css/swiper.min.css'
	])
		.pipe(changed(paths.dest + 'css'))
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(cssnano())
		.pipe(concat('libs.min.css'))
		.pipe(gulp.dest(paths.dest + 'css'));

	// JS Libs
	gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/swiper/dist/js/swiper.min.js'
	])
		.pipe(changed(paths.dest + 'js'))
		.pipe(concat('libs.min.js'))
		.pipe(gulp.dest(paths.dest + 'js'));
});

gulp.task('watch', function() {
	var php = gulp.watch([paths.src + '**/*.php', paths.src + '**/*.html'], ['php']),
			css = gulp.watch([paths.src + 'scss/**/*.scss'], ['css']),
			js = gulp.watch([paths.src + 'js/**/*.js'], ['js']);

	browsersync.init([paths.dest], {
		"notify": false,
		server: {
			baseDir: "./../",
			serveStaticOptions: {
					extensions: ["html"]
			}
		}
	});
});

/*
*
* SERVER TASK
*
*/
gulp.task('server', ['php', 'css', 'js', 'img', 'libs']);

/*
*
* DEFAULT TASK
*
*/
gulp.task('default', ['php', 'css', 'js', 'img', 'libs', 'watch']);