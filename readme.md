# Basic html project

Basic html project using Gulp to build the application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to run the app and how to install them

```
NodeJs
Gulp
```

### Installing

```
After install Node and BASH(Windows):
- Run terminal into folder "app"
In the command line follow the instructions below:
- "npm install"
- "gulp" (Here you will run gulp default task, this will build your application and open in your default browser)
```


### Applied Technologies

Below some of the technologies used in the project

```
- Gulp
- Npm
- SCSS
- Autoprefixer
- Browsersync
- Imagemin
- CSSnano
```

## Deployment

In your pipeline run gulp server into app folder, than remove the app folder. 

## Author

* **Renam Freitas** - [Personal Page](https://renam.com.br/)
